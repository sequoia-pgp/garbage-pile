{ lib, python3Packages }:

python3Packages.buildPythonApplication {
  pname = "wkd-domain-checker";
  version = "1.0";

  propagatedBuildInputs = with python3Packages; [
    flask
    publicsuffix2
    requests
  ];

  src = ./.;

  meta = with lib; {
    description = "WKD domain checker for hagrid wkd gateway";
    homepage = "https://gitlab.com/keys.openpgp.org/hagrid";
    license = with licenses; [ gpl3 ];
    maintainers = with maintainers; [ valodim ];
    platforms = platforms.all;
  };
}
